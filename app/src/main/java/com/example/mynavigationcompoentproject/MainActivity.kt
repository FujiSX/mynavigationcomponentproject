package com.example.mynavigationcompoentproject

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private lateinit var navController: NavController

    override fun onBackPressed() {
        val navHost =
            supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        val target = navHost.childFragmentManager.findFragmentById(R.id.nav_host_fragment)
        if (target is OnBackPressHandler) {
            if (target.onBackPressed()) {
                return
            }
        }
        super.onBackPressed()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        navController = NavHostFragment.findNavController(nav_host_fragment)
        navController.addOnDestinationChangedListener { _, destination, _ ->
            setupToolbar(destination)
        }
        toolbar.setupWithNavController(navController)
    }

    private fun setupToolbar(destination: NavDestination) {
        when (destination.id) {
            // ここで指定されたFragmentではToolbarを表示しない
            R.id.oneFragment,
            R.id.secondFragment,
            R.id.thridFragment -> {
                toolbar.visibility = View.GONE
            }
            else -> {
                toolbar.visibility = View.VISIBLE
            }
        }
    }

    interface OnBackPressHandler {
        fun onBackPressed(): Boolean
    }
}