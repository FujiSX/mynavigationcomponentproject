package com.example.mynavigationcompoentproject.second_nav

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.example.mynavigationcompoentproject.R
import com.example.mynavigationcompoentproject.databinding.FragmentFirstBinding
import com.example.mynavigationcompoentproject.databinding.FragmentSecondBinding

class SecondFragment : Fragment() {
    private lateinit var viewDataBinding: FragmentSecondBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        viewDataBinding = FragmentSecondBinding.inflate(inflater, container, false)
        return viewDataBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewDataBinding.buttonSecond.setOnClickListener {
            view?.findNavController()?.navigate(R.id.action_secondFragment_to_thridFragment)
        }
    }
}