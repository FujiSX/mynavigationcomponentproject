package com.example.mynavigationcompoentproject.second_nav

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import com.example.mynavigationcompoentproject.R
import com.example.mynavigationcompoentproject.databinding.FragmentThirdBinding

class ThridFragment : Fragment() {

    private lateinit var viewDataBinding: FragmentThirdBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        viewDataBinding = FragmentThirdBinding.inflate(inflater, container, false)
        return viewDataBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewDataBinding.buttonThird.setOnClickListener {
            NavHostFragment.findNavController(this).popBackStack(R.id.nav_first, true)
        }
    }
}