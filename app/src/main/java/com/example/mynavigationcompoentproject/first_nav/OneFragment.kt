package com.example.mynavigationcompoentproject.first_nav

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.example.mynavigationcompoentproject.R
import com.example.mynavigationcompoentproject.databinding.FragmentFirstBinding

class OneFragment : Fragment() {

    private lateinit var viewDataBinding: FragmentFirstBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        viewDataBinding = FragmentFirstBinding.inflate(inflater, container, false)
        return viewDataBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewDataBinding.buttonFirst.setOnClickListener {
            view?.findNavController()?.navigate(R.id.action_global_nav_second)
        }
    }
}